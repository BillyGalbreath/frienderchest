package net.pl3x.bukkit.frienderchest.configuration;

import net.pl3x.bukkit.frienderchest.FrienderChest;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

/**
 * Utility for language support
 */
public class Lang {
    public static String COMMAND_NO_PERMISSION = "&4You do not have permission for that command!";
    public static String PLAYER_COMMAND = "&4This command is only available to players.";
    public static String USER_NOT_FOUND = "&4Cannot find that user!";
    public static String FRIENDERCHEST_TITLE = "&b{player}'s &aEnder Chest";
    public static String OPENED_OTHER_FRIENDERCHEST = "&dOpened {player}'s ender chest.";
    public static String OPENED_FRIENDERCHEST = "&dOpened your ender chest.";
    public static String RELOAD = "&d{plugin} v{version} reloaded.";

    /**
     * Reload the language values from disk
     */
    public static void reload() {
        FrienderChest plugin = FrienderChest.getPlugin();
        String langFile = Config.LANGUAGE_FILE;
        File configFile = new File(plugin.getDataFolder(), langFile);
        if (!configFile.exists()) {
            plugin.saveResource(Config.LANGUAGE_FILE, false);
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

        COMMAND_NO_PERMISSION = config.getString("command-no-permission", "&4You do not have permission for that command!");
        PLAYER_COMMAND = config.getString("player-command", "&4This command is only available to players.");
        USER_NOT_FOUND = config.getString("user-not-found", "&4Cannot find that user!");
        FRIENDERCHEST_TITLE = config.getString("frienderchest-title", "&b{player}'s &aEnder Chest");
        OPENED_OTHER_FRIENDERCHEST = config.getString("opened-other-enderchest", "&dOpened {player}'s ender chest.");
        OPENED_FRIENDERCHEST = config.getString("opened-enderchest", "&dOpened your ender chest.");
        RELOAD = config.getString("reload", "&d{plugin} v{version} reloaded.");
    }
}
