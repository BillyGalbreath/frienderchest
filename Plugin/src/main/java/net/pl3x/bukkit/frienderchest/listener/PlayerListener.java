package net.pl3x.bukkit.frienderchest.listener;

import net.pl3x.bukkit.frienderchest.Chat;
import net.pl3x.bukkit.frienderchest.FrienderChest;
import net.pl3x.bukkit.frienderchest.Logger;
import net.pl3x.bukkit.frienderchest.configuration.Lang;
import net.pl3x.bukkit.frienderchest.enderchest.EnderChest;
import net.pl3x.bukkit.frienderchest.hook.VaultHook;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerListener implements Listener {
    private final FrienderChest plugin;

    public PlayerListener(FrienderChest plugin) {
        this.plugin = plugin;
    }

    /*
     * Handle loading items in rows 4-6 if enderchest is larger than normal
     * (prevents rows 4-6 from losing their data, and fixes out of sync issues when trying to re-sync those rows later)
     */
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        // check if resize needed
        int allowedSize = VaultHook.getSize(player) * 9;
        if (allowedSize <= 27) {
            // not larger than normal. ignore.
            // only resize here if growing above normal size
            // so we dont delete/drop player's items for no reason
            return;
        }

        // resize the enderchest
        plugin.getNMS().resizeEnderChest(player, allowedSize);

        // reload NBT from disk to populate items in rows 4-6
        plugin.getNMS().reloadNBTFromDisk(player);
    }

    /*
     * Handle resizing player's own enderchest when opening by clicking enderchest in world
     */
    @EventHandler
    public void onOpenInventory(InventoryOpenEvent event) {
        if (event.getInventory().getType() != InventoryType.ENDER_CHEST) {
            return; // not an enderchest
        }

        if (!(event.getPlayer() instanceof Player)) {
            return; // not a player
        }

        Player player = (Player) event.getPlayer();
        String title = ChatColor.stripColor(event.getInventory().getTitle());
        if (title.contains("'s")) {
            // check if opening other
            if (!player.getName().equals(title.split("'s")[0])) {
                return; // other already processed by command
            }
        }

        // get own enderchest
        EnderChest enderChest = plugin.getEnderChestManager().getEnderChest(player);
        if (enderChest == null) {
            Logger.debug(player.getName() + " could not open correct EnderChest! " + event.getPlayer().getLocation());
            event.setCancelled(true);
            return;
        }

        // check if already viewing this enderchest
        if (enderChest.getViewers().contains(player)) {
            return; // already looking at inventory
        }

        // cancel this enderchest and re-open the newly sized enderchest
        event.setCancelled(true);
        enderChest.open(player);
        new Chat(Lang.OPENED_FRIENDERCHEST).send(player);
    }

    /*
     * handles immediately saving offline enderchests when they are closed
     */
    @EventHandler
    public void onCloseInventory(InventoryCloseEvent event) {
        if (event.getInventory().getType() != InventoryType.ENDER_CHEST) {
            return; // not an enderchest
        }

        if (!(event.getPlayer() instanceof Player)) {
            return; // not a player
        }

        EnderChest enderChest = plugin.getEnderChestManager().getEnderChest(event.getInventory());
        if (enderChest == null) {
            Logger.debug(event.getPlayer().getName() + " closed EnderChest that was not being tracked! " + event.getPlayer().getLocation());
            return;
        }

        plugin.getEnderChestManager().close(enderChest, (Player) event.getPlayer());
    }
}
