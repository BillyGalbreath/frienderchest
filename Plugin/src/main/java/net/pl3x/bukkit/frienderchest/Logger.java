package net.pl3x.bukkit.frienderchest;

import net.pl3x.bukkit.frienderchest.configuration.Config;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class Logger {
    private static void log(String msg) {
        msg = ChatColor.translateAlternateColorCodes('&', "&3[&d" + FrienderChest.getPlugin().getName() + "&3]&r " + msg);
        if (!Config.COLOR_LOGS) {
            msg = ChatColor.stripColor(msg);
        }
        Bukkit.getServer().getConsoleSender().sendMessage(msg);
    }

    public static void debug(String msg) {
        if (Config.DEBUG_MODE) {
            Logger.log("&7[&eDEBUG&7]&e " + msg);
        }
    }

    static void error(String msg) {
        Logger.log("&e[&4ERROR&e]&4 " + msg);
    }

    static void info(String msg) {
        Logger.log("&e[&fINFO&e]&r " + msg);
    }
}
