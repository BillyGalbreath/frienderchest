package net.pl3x.bukkit.frienderchest.command;

import net.pl3x.bukkit.frienderchest.Chat;
import net.pl3x.bukkit.frienderchest.FrienderChest;
import net.pl3x.bukkit.frienderchest.configuration.Config;
import net.pl3x.bukkit.frienderchest.configuration.Lang;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CmdFrienderChest implements TabExecutor {
    private final FrienderChest plugin;

    public CmdFrienderChest(FrienderChest plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1) {
            List<String> options = new ArrayList<>();
            // only players can tab complete player names
            if (sender instanceof Player && sender.hasPermission("command.frienderchest.other")) {
                options.addAll(Bukkit.getOnlinePlayers().stream()
                        .map((Function<Player, String>) HumanEntity::getName)
                        .collect(Collectors.toList()));
            }
            // only console and players with permission can reload plugin
            if (sender.hasPermission("command.frienderchest.reload")) {
                options.add("reload");
            }
            return options.stream()
                    .filter(option -> option.toLowerCase().startsWith(args[0].toLowerCase()))
                    .collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        // reload configs command
        if (args.length == 1 && args[0].equalsIgnoreCase("reload")) {
            if (!sender.hasPermission("command.frienderchest.reload")) {
                new Chat(Lang.COMMAND_NO_PERMISSION).send(sender);
                return true;
            }

            Config.reload();
            Lang.reload();

            plugin.getEnderChestManager().closeAll();

            plugin.getNMS().setDebugColors(Config.DEBUG_MODE, Config.COLOR_LOGS);

            new Chat(Lang.RELOAD
                    .replace("{plugin}", plugin.getName())
                    .replace("{version}", plugin.getDescription().getVersion()))
                    .send(sender);
            return true;
        }

        // only players can open enderchests...
        if (!(sender instanceof Player)) {
            new Chat(Lang.PLAYER_COMMAND).send(sender);
            return true;
        }

        // get opener and owner
        Player player = (Player) sender;
        if (args.length == 1 && !args[0].equalsIgnoreCase(player.getName())) {
            // handle another player's enderchest
            // do not handle typing self's name in command
            if (!sender.hasPermission("command.frienderchest.other")) {
                new Chat(Lang.COMMAND_NO_PERMISSION).send(sender);
                return true;
            }

            // get offline player
            OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args[0]);
            if (offlinePlayer == null) {
                new Chat(Lang.USER_NOT_FOUND).send(sender);
                return true;
            }

            if (offlinePlayer.isOnline()) {
                plugin.getEnderChestManager()
                        .getEnderChest(offlinePlayer.getPlayer())
                        .open(player);
                new Chat(Lang.OPENED_OTHER_FRIENDERCHEST
                        .replace("{player}", offlinePlayer.getName()))
                        .send(sender);
                return true;
            }

            Player fakePlayer = plugin.getNMS().getOfflinePlayer(offlinePlayer.getUniqueId());
            if (fakePlayer == null) {
                new Chat(Lang.USER_NOT_FOUND).send(sender);
                return true;
            }

            plugin.getEnderChestManager()
                    .getEnderChest(fakePlayer)
                    .open(player);
            new Chat(Lang.OPENED_OTHER_FRIENDERCHEST
                    .replace("{player}", fakePlayer.getName()))
                    .send(sender);
            return true;
        }

        // opening self's enderchest
        if (!sender.hasPermission("command.frienderchest")) {
            new Chat(Lang.COMMAND_NO_PERMISSION).send(sender);
            return true;
        }

        // open enderchest
        plugin.getEnderChestManager()
                .getEnderChest(player)
                .open(player);
        new Chat(Lang.OPENED_FRIENDERCHEST).send(sender);
        return true;
    }
}
