package net.pl3x.bukkit.frienderchest;

import net.pl3x.bukkit.frienderchest.api.NMS;
import net.pl3x.bukkit.frienderchest.command.CmdFrienderChest;
import net.pl3x.bukkit.frienderchest.configuration.Config;
import net.pl3x.bukkit.frienderchest.configuration.Lang;
import net.pl3x.bukkit.frienderchest.enderchest.EnderChestManager;
import net.pl3x.bukkit.frienderchest.hook.VaultHook;
import net.pl3x.bukkit.frienderchest.listener.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class FrienderChest extends JavaPlugin {
    private NMS nmsHandler;
    private EnderChestManager enderChestManager;

    @Override
    public void onEnable() {
        Config.reload();
        Lang.reload();

        if (!hasSupportedVersion()) {
            String version = getServer().getBukkitVersion().split("_")[0].split("-")[0];
            error("#         Could not find support for server version &3" + version + "&4!                #",
                    "#                    Supported versions are 1.8.0 - 1.10.2                    #");
            return;
        }

        if (!getServer().getPluginManager().isPluginEnabled("Vault")) {
            error("#                  Dependency plugin, &3Vault&4, was not found.                   #",
                    "#                https://dev.bukkit.org/bukkit-plugins/vault/                 #");
            return;
        }

        if (VaultHook.failedPermissionSetup()) {
            error("#            Vault could not find a compatible permission plugin.              #",
                    "#          We recommend zPermissions, which can be downloaded here:           #",
                    "#           https://www.spigotmc.org/resources/zpermissions.11736/            #");
            return;
        }

        getNMS().setDebugColors(Config.DEBUG_MODE, Config.COLOR_LOGS);

        enderChestManager = new EnderChestManager();

        Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);

        getCommand("frienderchest").setExecutor(new CmdFrienderChest(this));

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        if (getEnderChestManager() != null) {
            getEnderChestManager().closeAll();
        }

        Logger.info(getName() + " Disabled.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4" + getName() + " is disabled. See console log for more information."));
        return true;
    }

    /**
     * Get plugin's main class instance
     *
     * @return Main class instance
     */
    public static FrienderChest getPlugin() {
        return FrienderChest.getPlugin(FrienderChest.class);
    }

    /**
     * Get instance to NMS handler
     *
     * @return NMS handler instance
     */
    public NMS getNMS() {
        return nmsHandler;
    }

    /**
     * Get instance to EnderChestManager
     *
     * @return EnderChestManager instance
     */
    public EnderChestManager getEnderChestManager() {
        return enderChestManager;
    }

    /**
     * Check for supported server version
     *
     * @return True if server is supported
     */
    private boolean hasSupportedVersion() {
        String packageName = this.getServer().getClass().getPackage().getName();
        String version = packageName.substring(packageName.lastIndexOf('.') + 1);
        try {
            final Class<?> nmsClazz = Class.forName("net.pl3x.bukkit.frienderchest.nms." + version + ".NMSHandler");
            if (NMS.class.isAssignableFrom(nmsClazz)) {
                nmsHandler = (NMS) nmsClazz.getConstructor().newInstance();
                Logger.debug("Registered NMSHandler for version " + version);
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * Display fancy error to console
     *
     * @param message Error message
     */
    private void error(String... message) {
        Logger.error("###############################################################################");
        for (String msg : message) {
            Logger.error(msg);
        }
        Logger.error("#                                                                             #");
        Logger.error("#     To prevent server crashes and other undesired behavior this plugin      #");
        Logger.error("#       is disabling itself from running. Please remove the plugin jar        #");
        Logger.error("#       from your plugins directory to free up the registered commands.       #");
        Logger.error("###############################################################################");
    }
}
