package net.pl3x.bukkit.frienderchest.enderchest;

import net.pl3x.bukkit.frienderchest.FrienderChest;
import net.pl3x.bukkit.frienderchest.api.NMS;
import net.pl3x.bukkit.frienderchest.configuration.Lang;
import net.pl3x.bukkit.frienderchest.hook.VaultHook;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.Collection;
import java.util.HashSet;

/**
 * Represents an EnderChest owned by a player
 */
public class EnderChest {
    private final NMS nms;
    private final Player owner;
    private final Inventory inventory;
    private final Collection<Player> viewers;
    private final boolean offline;

    /**
     * Constructs a new EnderChest with given owner
     *
     * @param owner Owner of this EnderChest
     */
    EnderChest(Player owner) {
        this.owner = owner;
        this.inventory = owner.getEnderChest();
        this.viewers = new HashSet<>();

        nms = FrienderChest.getPlugin().getNMS();

        // rename enderchest to include owner's name
        rename(Lang.FRIENDERCHEST_TITLE.replace("{player}", owner.getName()));

        // resize inventory to permission allowed
        resize();

        // load offline player data from disk
        if (!owner.isOnline()) {
            nms.reloadNBTFromDisk(owner);
            offline = true;
        } else {
            offline = false;
        }
    }

    /**
     * Get the owner of this EnderChest
     *
     * @return Owner of this EnderChest
     */
    public Player getOwner() {
        return owner;
    }

    /**
     * Get the inventory object
     *
     * @return Inventory
     */
    public Inventory getInventory() {
        return inventory;
    }

    /**
     * Get the current viewers viewing this EnderChest
     *
     * @return Collection of viewing Players
     */
    public Collection<Player> getViewers() {
        return viewers;
    }

    /**
     * Check if player is the owner of this EnderChest
     *
     * @param player Player to check
     * @return True if player owns this EnderChest
     */
    public boolean isOwner(Player player) {
        return getOwner().getUniqueId().equals(player.getUniqueId());
    }

    /**
     * Check if this EnderChest was loaded when owner was offline
     *
     * @return True if EnderChest was loaded when owner was offline
     */
    public boolean isOffline() {
        return offline;
    }

    /**
     * Add player as viewer and open EnderChest to their view
     *
     * @param player Player to open EnderChest
     */
    public void open(Player player) {
        getViewers().add(player);
        Bukkit.getScheduler().runTaskLater(FrienderChest.getPlugin(),
                () -> player.openInventory(getInventory()), 1);
    }

    /**
     * Remove player from viewers and close inventory view
     * <p>
     * If this is the last viewer the EnderChest contents will save to disk
     *
     * @param player Player closing EnderChest
     */
    void close(Player player) {
        getViewers().remove(player);
        if (isOffline() && getViewers().isEmpty()) {
            save();
        }
        if (getViewers().isEmpty()) {
            rename("Ender Chest");
        }
    }

    /**
     * Save EnderChest contents to disk
     */
    private void save() {
        nms.saveData(owner);
    }

    /**
     * Resize EnderChest inventory to current permissions if needed
     */
    void resize() {
        int size = VaultHook.getSize(owner) * 9;
        if (size != getInventory().getSize()) {
            nms.resizeEnderChest(owner, size);
        }
    }

    private void rename(String name) {
        nms.renameEnderChest(getInventory(), ChatColor.translateAlternateColorCodes('&', name));
    }
}
