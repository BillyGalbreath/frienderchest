package net.pl3x.bukkit.frienderchest.configuration;

import net.pl3x.bukkit.frienderchest.FrienderChest;
import org.bukkit.configuration.file.FileConfiguration;

/**
 * Utility for config.yml
 */
public class Config {
    public static boolean COLOR_LOGS = true;
    public static boolean DEBUG_MODE = false;
    public static String LANGUAGE_FILE = "lang-en.yml";

    /**
     * Reload the config.yml values from disk
     */
    public static void reload() {
        FrienderChest plugin = FrienderChest.getPlugin();
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");
    }
}
