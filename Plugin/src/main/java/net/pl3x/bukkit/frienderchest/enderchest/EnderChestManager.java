package net.pl3x.bukkit.frienderchest.enderchest;

import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.HashSet;
import java.util.Set;

/**
 * Manages the EnderChest instances
 */
public class EnderChestManager {
    private final Set<EnderChest> openEnderChests = new HashSet<>();

    /**
     * Get the EnderChest for given player
     * <p>
     * If player's EnderChest is already open by another player the same instance
     * will be returned to share the same View for real time updates
     *
     * @param owner Owning Player
     * @return EnderChest belonging to owner
     */
    public EnderChest getEnderChest(Player owner) {
        for (EnderChest enderChest : openEnderChests) {
            if (enderChest.isOwner(owner)) {
                enderChest.resize();
                return enderChest;
            }
        }
        EnderChest enderChest = new EnderChest(owner);
        openEnderChests.add(enderChest);
        return enderChest;
    }

    /**
     * Get an already open EnderChest by Inventory
     *
     * @param inventory Inventory to check
     * @return EnderChest with given Inventory, or null if no open EnderChest with matching Inventory found
     */
    public EnderChest getEnderChest(Inventory inventory) {
        for (EnderChest enderChest : openEnderChests) {
            if (enderChest.getInventory().getName().equals(inventory.getName())) {
                enderChest.resize();
                return enderChest;
            }
        }
        return null;
    }

    /**
     * Close an EnderChest from player's view
     *
     * @param enderChest EnderChest to close
     * @param player     Player to close on
     */
    public void close(EnderChest enderChest, Player player) {
        enderChest.close(player);
        if (enderChest.getViewers().isEmpty()) {
            openEnderChests.remove(enderChest);
        }
    }

    /**
     * Close all open EnderChests
     */
    public void closeAll() {
        Set<EnderChest> tmpChests = new HashSet<>(openEnderChests);
        for (EnderChest enderChest : tmpChests) {
            enderChest.getViewers().forEach(HumanEntity::closeInventory);
        }
        openEnderChests.clear();
    }
}
