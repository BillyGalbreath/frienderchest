package net.pl3x.bukkit.frienderchest;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class Chat {
    private final String message;

    /**
     * Construct new chat message object
     *
     * @param message Message contents
     */
    public Chat(String message) {
        this.message = ChatColor.translateAlternateColorCodes('&', message);
    }

    /**
     * Send chat message
     * <p>
     * Line breaks will be split into multiple chat messages sent
     * <p>
     * Blank messages will not be sent (including those that are only color codes)
     *
     * @param recipient Recipient to receive message
     */
    public void send(CommandSender recipient) {
        if (message == null || ChatColor.stripColor(message).isEmpty()) {
            return; // do not send blank messages
        }

        for (String part : message.split("\n")) {
            recipient.sendMessage(part);
        }
    }
}
