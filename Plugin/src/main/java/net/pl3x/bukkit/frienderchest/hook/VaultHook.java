package net.pl3x.bukkit.frienderchest.hook;

import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

public class VaultHook {
    private static Permission permission;

    /**
     * Check is Vault can setup a permissions hook
     *
     * @return True if no compatible permissions plugin found
     */
    public static boolean failedPermissionSetup() {
        return (permission = Bukkit.getServicesManager().getRegistration(Permission.class).getProvider()) == null;
    }

    /**
     * Get a player's allowed number of enderchest rows
     *
     * @param player OfflinePlayer
     * @return Number of enderchest rows allowed
     */
    public static int getSize(OfflinePlayer player) {
        for (int i = 6; i > 0; i--) {
            if (permission.playerHas(null, player, "frienderchest.size." + i)) {
                return i;
            }
        }
        return 3; // default to 3
    }
}
