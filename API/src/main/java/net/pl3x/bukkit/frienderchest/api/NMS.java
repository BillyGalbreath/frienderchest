package net.pl3x.bukkit.frienderchest.api;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.UUID;

public interface NMS {
    /**
     * Set debug and console color modes
     *
     * @param debug  Debug mode
     * @param colors Console color mode
     */
    void setDebugColors(boolean debug, boolean colors);

    /**
     * Get a fake Player from UUID if they've played before
     *
     * @param uuid UUID of player to get
     * @return Faked online Player object
     */
    Player getOfflinePlayer(UUID uuid);

    /**
     * Saves a player's data to disk
     *
     * @param player Player to save
     */
    void saveData(Player player);

    /**
     * Resize a player's enderchest
     *
     * @param player      Owner of enderchest
     * @param allowedSize New size of enderchest (in slots, not rows)
     */
    void resizeEnderChest(Player player, int allowedSize);

    /**
     * Rename a player's enderchest
     *
     * @param enderChest Inventory to rename
     * @param name       New name
     */
    void renameEnderChest(Inventory enderChest, String name);

    /**
     * Reload a player's data from disk
     *
     * @param player Player to load
     */
    void reloadNBTFromDisk(Player player);
}
