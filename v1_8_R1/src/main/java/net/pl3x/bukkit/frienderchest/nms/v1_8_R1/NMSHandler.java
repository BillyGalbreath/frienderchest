package net.pl3x.bukkit.frienderchest.nms.v1_8_R1;

import com.mojang.authlib.GameProfile;
import net.minecraft.server.v1_8_R1.EntityItem;
import net.minecraft.server.v1_8_R1.EntityPlayer;
import net.minecraft.server.v1_8_R1.InventoryEnderChest;
import net.minecraft.server.v1_8_R1.InventorySubcontainer;
import net.minecraft.server.v1_8_R1.MinecraftServer;
import net.minecraft.server.v1_8_R1.NBTTagCompound;
import net.minecraft.server.v1_8_R1.PlayerInteractManager;
import net.minecraft.server.v1_8_R1.PlayerList;
import net.pl3x.bukkit.frienderchest.api.NMS;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.craftbukkit.v1_8_R1.CraftServer;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftHumanEntity;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R1.inventory.CraftInventory;
import org.bukkit.craftbukkit.v1_8_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.UUID;

public class NMSHandler implements NMS {
    private boolean debug = false;
    private boolean colors = true;

    public void setDebugColors(boolean debug, boolean colors) {
        this.debug = debug;
        this.colors = colors;
    }

    @Override
    public Player getOfflinePlayer(UUID uuid) {
        OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(uuid);
        if (offlinePlayer == null || !offlinePlayer.hasPlayedBefore()) {
            // no data to load
            return null;
        }
        MinecraftServer server = ((CraftServer) Bukkit.getServer()).getServer();
        // create fake EntityPlayer and convert into Bukkit Player
        return new EntityPlayer(server, server.getWorldServer(0),
                new GameProfile(uuid, offlinePlayer.getName()),
                new PlayerInteractManager(server.getWorldServer(0)))
                .getBukkitEntity();
    }

    @Override
    public void saveData(Player player) {
        if (player != null && !player.isOnline()) {
            try {
                // save only if offline
                //player.saveData();
                PlayerList playerList = ((CraftServer) Bukkit.getServer()).getServer().getPlayerList();
                Method savePlayerFile = PlayerList.class.getDeclaredMethod("savePlayerFile", EntityPlayer.class);
                savePlayerFile.setAccessible(true);
                savePlayerFile.invoke(playerList, ((CraftPlayer) player).getHandle());

                debug("Saved " + player.getName() + "'s offline enderchest.");
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void resizeEnderChest(Player player, int allowedSize) {
        // the dirty hacks
        try {
            // drop extra contents if shrinking
            ItemStack[] contents = player.getEnderChest().getContents();
            if (allowedSize < contents.length) {
                for (int i = allowedSize; i < contents.length; i++) {
                    if (contents[i] != null && contents[i].getType() != Material.AIR) {
                        EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();
                        EntityItem entityItem = entityPlayer.drop(CraftItemStack.asNMSCopy(contents[i]), false);
                        if (entityItem != null) {
                            entityItem.q(); // pickupDelay = 0
                            entityItem.b(player.getName()); // owner name
                        }
                    }
                }
            }

            // update enderchest size
            InventoryEnderChest inventory = (InventoryEnderChest) ((CraftInventory) player.getEnderChest()).getInventory();
            Field b = InventorySubcontainer.class.getDeclaredField("b"); // inventory size
            b.setAccessible(true);
            b.set(inventory, allowedSize);

            // update itemstacks
            Field items = InventorySubcontainer.class.getDeclaredField("items");
            items.setAccessible(true);
            items.set(inventory, Arrays.copyOf(inventory.getContents(), allowedSize));

            // update craftbukkit copy
            CraftPlayer craftPlayer = (CraftPlayer) player;
            Field enderChest = CraftHumanEntity.class.getDeclaredField("enderChest");
            enderChest.setAccessible(true);
            enderChest.set(craftPlayer, new CraftInventory(inventory));

            debug("Resized " + player.getName() + "'s offline enderchest (" + inventory.getSize() + ")");
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void renameEnderChest(Inventory enderChest, String name) {
        try {
            // change inventory's name
            InventoryEnderChest inventory = (InventoryEnderChest) ((CraftInventory) enderChest).getInventory();
            Field a = InventorySubcontainer.class.getDeclaredField("a"); // inventory name
            a.setAccessible(true);
            a.set(inventory, name);

            // set custom name boolean
            Field e = InventorySubcontainer.class.getDeclaredField("e"); // has custom name
            e.setAccessible(true);
            e.set(inventory, !name.equals("Ender Chest"));

            debug("Renamed enderchest: " + name);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void reloadNBTFromDisk(Player player) {
        // load _all_ nbt data from disk
        NBTTagCompound nbt = ((CraftServer) Bukkit.getServer()).getServer().getPlayerList().playerFileData.load(((CraftPlayer) player).getHandle());
        if (nbt == null) {
            return; // no stored nbt to load for this player (new player, etc)
        }

        // we only care about ender chest stuff
        if (nbt.hasKeyOfType("EnderItems", 9)) {
            // set the items from nbt into player's enderchest
            InventoryEnderChest inventory = (InventoryEnderChest) ((CraftInventory) player.getEnderChest()).getInventory();
            inventory.a(nbt.getList("EnderItems", 10)); // load items from nbt data

            // count the items (for debug purposes only)
            if (debug) {
                int count = 0;
                for (net.minecraft.server.v1_8_R1.ItemStack stack : inventory.getContents()) {
                    if (stack != null) {
                        count++;
                    }
                }

                debug("Reloaded " + player.getName() + "'s offline enderchest contents (" + count + ")");
            }
        }
    }

    private void debug(String message) {
        if (debug) {
            message = ChatColor.translateAlternateColorCodes('&', "&3[&dFrienderChest&3]&7[&eDEBUG&7]&e " + message);
            if (!colors) {
                message = ChatColor.stripColor(message);
            }
            Bukkit.getConsoleSender().sendMessage(message);
        }
    }
}
